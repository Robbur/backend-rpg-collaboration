package main.java;

import main.java.basestats.ItemRarityModifiers;
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.CharacterType;
import main.java.characters.damage.Mage;
import main.java.characters.damage.Paladin;
import main.java.characters.damage.Warrior;
import main.java.characters.support.Druid;
import main.java.factories.ArmorFactory;
import main.java.factories.CharacterFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponType;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        DecimalFormat df = new DecimalFormat("###.00");

        WeaponFactory weaponFactory = new WeaponFactory();
        ArmorFactory armorFactory = new ArmorFactory();
        CharacterFactory characterFactory = new CharacterFactory();

        ArrayList<Character> partyList = new ArrayList<>();
        Mage mage = (Mage) characterFactory.getCharacter(CharacterType.Mage);
        Druid druid = (Druid) characterFactory.getCharacter(CharacterType.Druid);
        Warrior warrior = (Warrior) characterFactory.getCharacter(CharacterType.Warrior);
        Paladin paladin = (Paladin) characterFactory.getCharacter(CharacterType.Paladin);

        partyList.add(mage);
        partyList.add(druid);
        partyList.add(warrior);
        partyList.add(paladin);

        System.out.println("Party list consists of: ");
        for (Character c : partyList) {
            System.out.println("A " + c.getCHARACTER_TYP() + " wearing " + c.getARMOR_TYPE() + " armor");
        }

        System.out.println();

        System.out.println("ATTACKS & HEALS WITH STANDARD WEAPONS:");
        System.out.println("Mage attacks with " + df.format(mage.castDamagingSpell()) + " damage");
        System.out.println("Druid heals mage for " + df.format(druid.healPartyMember(mage)) + " hitpoints");
        System.out.println("Warrior attacks with " + df.format(warrior.attackWithBladedWeapon()) + " damage");
        System.out.println("Paladin attacks with " + df.format(paladin.attackWithBluntWeapon()) + " damage");

        System.out.println();

        System.out.println("TAKING DAMAGE WITH STANDARD ARMOR EQUIPPED:\n");
        System.out.println("MAGE:");
        System.out.println("Mage receiving 100 physical damage: ");
        System.out.println("Actual damage received: " + mage.takeDamage(100, "physical"));
        System.out.println("Mage receiving 100 magical damage: ");
        System.out.println("Actual damage received: " + mage.takeDamage(100, "magical"));

        System.out.println();

        System.out.println("DRUID:");
        System.out.println("Druid receiving 100 physical damage: ");
        System.out.println("Actual damage received: " + druid.takeDamage(100, "physical"));
        System.out.println("Druid receiving 100 magical damage: ");
        System.out.println("Actual damage received: " + druid.takeDamage(100, "magical"));

        System.out.println();

        System.out.println("WARRIOR:");
        System.out.println("Warrior receiving 100 physical damage: ");
        System.out.println("Actual damage received: " + warrior.takeDamage(100, "physical"));
        System.out.println("Warrior receiving 100 magical damage: ");
        System.out.println("Actual damage received: " + warrior.takeDamage(100, "magical"));

        System.out.println();

        System.out.println("PALADIN:");
        System.out.println("Paladin receiving 100 physical damage: ");
        System.out.println("Actual damage received: " + paladin.takeDamage(100, "physical"));
        System.out.println("Paladin receiving 100 magical damage: ");
        System.out.println("Actual damage received: " + paladin.takeDamage(100, "magical"));

        System.out.println();

        for(Character c : partyList){
            System.out.println(c.getCHARACTER_TYP() + " Health: " + df.format(c.getCurrentHealth()) + "/" + df.format(c.getCurrentMaxHealth()));
        }

        System.out.println("\nDruid heals mage for: " + df.format(druid.healPartyMember(mage)) + " hitpoints");
        System.out.println("Druid heals himself for: " + df.format(druid.healPartyMember(druid)) + " hitpoints");
        System.out.println("Druid heals paladin for: " + df.format(druid.healPartyMember(warrior)) + " hitpoints");
        System.out.println("Druid heals warrior for: " + df.format(druid.healPartyMember(paladin)) + " hitpoints");

        System.out.println();

        for(Character c : partyList){
            System.out.println(c.getCHARACTER_TYP() + " Health: " + df.format(c.getCurrentHealth()) + "/" + df.format(c.getCurrentMaxHealth()));
        }

        System.out.println();

        System.out.println("MAGE:");
        System.out.println("Current max health: " + df.format(mage.getCurrentMaxHealth()));
        mage.equipArmor(armorFactory.getArmor(ArmorType.Cloth, ItemRarityModifiers.LEGENDARY_RARITY_MODIFIER));
        mage.equipWeapon(weaponFactory.getWeapon(WeaponType.Staff, ItemRarityModifiers.LEGENDARY_RARITY_MODIFIER));
        System.out.println("New max health: " + df.format(mage.getCurrentMaxHealth()));

        System.out.println("\nDRUID:");
        System.out.println("Current max health: " + df.format(druid.getCurrentMaxHealth()));
        druid.equipArmor(armorFactory.getArmor(ArmorType.Leather, ItemRarityModifiers.LEGENDARY_RARITY_MODIFIER));
        druid.equipWeapon(weaponFactory.getWeapon(WeaponType.Staff, ItemRarityModifiers.LEGENDARY_RARITY_MODIFIER));
        System.out.println("New max health: " + df.format(druid.getCurrentMaxHealth()));

        System.out.println("\nPALADIN:");
        System.out.println("Current max health: " + df.format(paladin.getCurrentMaxHealth()));
        paladin.equipArmor(armorFactory.getArmor(ArmorType.Plate, ItemRarityModifiers.LEGENDARY_RARITY_MODIFIER));
        paladin.equipWeapon(weaponFactory.getWeapon(WeaponType.Hammer, ItemRarityModifiers.LEGENDARY_RARITY_MODIFIER));
        System.out.println("New max health: " + df.format(paladin.getCurrentMaxHealth()));

        System.out.println("\nWARRIOR:");
        System.out.println("Current max health: " + df.format(warrior.getCurrentMaxHealth()));
        warrior.equipArmor(armorFactory.getArmor(ArmorType.Plate, ItemRarityModifiers.LEGENDARY_RARITY_MODIFIER));
        warrior.equipWeapon(weaponFactory.getWeapon(WeaponType.Sword, ItemRarityModifiers.LEGENDARY_RARITY_MODIFIER));
        System.out.println("New max health: " + df.format(warrior.getCurrentMaxHealth()));

        System.out.println();

        System.out.println("ATTACKS & HEALS WITH BETTER WEAPONS:");
        System.out.println("Mage attacks with " + df.format(mage.castDamagingSpell()) + " damage");
        System.out.println("Druid heals mage for " + df.format(druid.healPartyMember(mage)) + " hitpoints");
        System.out.println("Warrior attacks with " + df.format(warrior.attackWithBladedWeapon()) + " damage");
        System.out.println("Paladin attacks with " + df.format(paladin.attackWithBluntWeapon()) + " damage");

        System.out.println();

        System.out.println("TAKING DAMAGE WITH NEW ARMOR EQUIPPED:\n");

        System.out.println("MAGE:");
        System.out.println("Mage receiving 100 physical damage: ");
        System.out.println("Actual damage received: " + mage.takeDamage(100, "physical"));
        System.out.println("Mage receiving 100 magical damage: ");
        System.out.println("Actual damage received: " + mage.takeDamage(100, "magical"));

        System.out.println();

        System.out.println("DRUID:");
        System.out.println("Druid receiving 100 physical damage: ");
        System.out.println("Actual damage received: " + druid.takeDamage(100, "physical"));
        System.out.println("Druid receiving 100 magical damage: ");
        System.out.println("Actual damage received: " + druid.takeDamage(100, "magical"));

        System.out.println();

        System.out.println("WARRIOR:");
        System.out.println("Warrior receiving 100 physical damage: ");
        System.out.println("Actual damage received: " + warrior.takeDamage(100, "physical"));
        System.out.println("Warrior receiving 100 magical damage: ");
        System.out.println("Actual damage received: " + warrior.takeDamage(100, "magical"));

        System.out.println();

        System.out.println("PALADIN:");
        System.out.println("Paladin receiving 100 physical damage: ");
        System.out.println("Actual damage received: " + paladin.takeDamage(100, "physical"));
        System.out.println("Paladin receiving 100 magical damage: ");
        System.out.println("Actual damage received: " + paladin.takeDamage(100, "magical"));

        System.out.println("\n");

        for(Character c : partyList){
            System.out.println(c.getCHARACTER_TYP() + " Health: " + df.format(c.getCurrentHealth()) + "/" + df.format(c.getCurrentMaxHealth()));
        }

        System.out.println("\n");

        System.out.println("HEALING WITH NEW ARMOR AND WEAPON\n");

        System.out.println("Druid heals mage for: " + df.format(druid.healPartyMember(mage)) + " hitpoints");
        System.out.println("Druid heals himself for: " + df.format(druid.healPartyMember(druid)) + " hitpoints");
        System.out.println("Druid heals paladin for: " + df.format(druid.healPartyMember(warrior)) + " hitpoints");
        System.out.println("Druid heals warrior for: " + df.format(druid.healPartyMember(paladin)) + " hitpoints");
        System.out.println();

        for(Character c : partyList){
            System.out.println(c.getCHARACTER_TYP() + " Health: " + df.format(c.getCurrentHealth()) + "/" + df.format(c.getCurrentMaxHealth()));
        }

        System.out.println("\nDruid heals mage for: " + df.format(druid.healPartyMember(mage)) + " hitpoints");
        System.out.println("Druid heals himself for: " + df.format(druid.healPartyMember(druid)) + " hitpoints");
        System.out.println("Druid heals paladin for: " + df.format(druid.healPartyMember(warrior)) + " hitpoints");
        System.out.println("Druid heals warrior for: " + df.format(druid.healPartyMember(paladin)) + " hitpoints");
        System.out.println();

        for(Character c : partyList){
            System.out.println(c.getCHARACTER_TYP() + " Health: " + df.format(c.getCurrentHealth()) + "/" + df.format(c.getCurrentMaxHealth()));
        }
    }
}
