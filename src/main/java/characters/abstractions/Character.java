package main.java.characters.abstractions;

import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponCategory;

public abstract class Character {

    // Metadata
    
    // Character Type
    protected CharacterType CHARACTER_TYP;
    // Armor Type
    protected ArmorType ARMOR_TYPE;
    // Weapon Type
    protected WeaponCategory WEAPON_TYPE;

    // Base stats defensive
    protected double baseHealth; // Base Health
    protected double basePhysReductionPercent; // Armor
    protected double baseMagicReductionPercent; // Magic armor

    public Character(CharacterType CHARACTER_TYP,
                     ArmorType ARMOR_TYPE,
                     WeaponCategory WEAPON_TYPE,
                     double baseHealth,
                     double basePhysReductionPercent,
                     double baseMagicReductionPercent) {
        this.CHARACTER_TYP = CHARACTER_TYP;
        this.ARMOR_TYPE = ARMOR_TYPE;
        this.WEAPON_TYPE = WEAPON_TYPE;
        this.baseHealth = baseHealth;
        this.basePhysReductionPercent = basePhysReductionPercent;
        this.baseMagicReductionPercent = baseMagicReductionPercent;
    }

    public abstract double getCurrentMaxHealth();

    public abstract double getCurrentHealth();

    public abstract void setCurrentHealth(double v);

    public abstract Boolean getDead();

    public abstract void equipArmor(Armor armor);

    public abstract void equipWeapon(Weapon weapon);

    public abstract double takeDamage(double incomingDamage, String damageType);

    // Returns character type (mage, druid, warrior etc)
    public CharacterType getCHARACTER_TYP() {
        return CHARACTER_TYP;
    }

    // Returns which armor type the character uses
    public ArmorType getARMOR_TYPE() {
        return ARMOR_TYPE;
    }

    // Returns which type of weapon the character uses
    public WeaponCategory getWEAPON_TYPE() {
        return WEAPON_TYPE;
    }

    // Returns the characters base health
    public double getBaseHealth() {
        return baseHealth;
    }

    // Returns the characters base physical reduction percent
    public double getBasePhysReductionPercent() {
        return basePhysReductionPercent;
    }

    // Returns the characters base magical reduction percent
    public double getBaseMagicReductionPercent() {
        return baseMagicReductionPercent;
    }
}
