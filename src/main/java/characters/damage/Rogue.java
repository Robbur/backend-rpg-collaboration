package main.java.characters.damage;
// Imports

import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.basestats.ItemRarityModifiers;
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.CharacterType;
import main.java.characters.abstractions.MeleeCharacter;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.abstractions.PlateArmor;
import main.java.items.armor.armors.Leather;
import main.java.items.weapons.abstractions.BladedWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.items.weapons.melee.Dagger;

/*
 Rogues are stealthy combatants of the shadows.
 They wield bladed weapons with great agility and dispatch their enemies swiftly.
*/
public class Rogue extends Character implements MeleeCharacter {

    // Active trackers and flags
    private double currentHealth;
    private double currentMaxHealth;
    private Boolean isDead = false;

    // Armor, Weapon
    public Armor equippedArmor = new Leather(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    public Weapon equippedWeapon = new Dagger(ItemRarityModifiers.COMMON_RARITY_MODIFIER);

    // Power
    private final double baseAttackPower = CharacterBaseStatsOffensive.ROGUE_MELEE_ATTACK_POWER;

    // Constructor
    public Rogue() {
        super(CharacterType.Rogue,
                ArmorType.Leather,
                WeaponCategory.BladeWeapon,
                CharacterBaseStatsDefensive.ROGUE_BASE_HEALTH,
                CharacterBaseStatsDefensive.ROGUE_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.ROGUE_BASE_MAGIC_RES);
        this.currentMaxHealth = baseHealth * equippedArmor.getHealthModifier() * equippedArmor.getRarityModifier();
        this.currentHealth = baseHealth * equippedArmor.getHealthModifier() * equippedArmor.getRarityModifier();
        // When the character is created it has maximum health (base health)
    }

    // Returns current max health
    @Override
    public double getCurrentMaxHealth() {
        return currentMaxHealth;
    }

    // Returns current health
    @Override
    public double getCurrentHealth() {
        return currentHealth;
    }

    // Change health when healed or shielded
    @Override
    public void setCurrentHealth(double incoming) {
        if (currentHealth + incoming >= currentMaxHealth) {
            currentHealth = currentMaxHealth;
        } else {
            currentHealth += incoming;
        }
    }

    // Returns true if character is dead, false otherwise
    @Override
    public Boolean getDead() {
        return isDead;
    }

    // Equipment behaviours

    // Equip new armor, changing max health
    @Override
    public void equipArmor(Armor armor) {
        if (armor instanceof PlateArmor) {
            equippedArmor = armor;
            currentMaxHealth = baseHealth * armor.getHealthModifier() * armor.getRarityModifier();
            System.out.println("Equipped " + armor.getArmorType() + " with rarity " + armor.getRarityModifier());
        } else {
            System.out.println("Cannot equip " + armor.getArmorType() + " armor. Rogues can only equip Leather armor!");
        }
    }

    // Equip a new weapon
    @Override
    public void equipWeapon(Weapon weapon) {
        if (weapon instanceof BladedWeapon) {
            equippedWeapon = weapon;
            System.out.println("Equipped " + weapon.getWeaponType() + " with rarity " + weapon.getItemRarity());
        } else {
            System.out.println("Cannot equip " + weapon.getWeaponType() + ". Rogues can only equip an Axe, a Sword or a Dagger!");
        }
    }
    // Character behaviours

    // Attacks with a bladed weapon
    public double attackWithBladedWeapon() {
        return baseAttackPower * equippedWeapon.getPowerModifier() * equippedWeapon.getItemRarity(); // Replaced with actual damage amount based on calculations
    }

    // Character takes damage, either physical or magical
    @Override
    public double takeDamage(double incomingDamage, String damageType) {
        // If the damage is physical
        if (damageType.equalsIgnoreCase("Physical")) {
            double damage = incomingDamage * (1 - (basePhysReductionPercent * equippedArmor.getPhysRedModifier() * equippedArmor.getRarityModifier()));
            // If damage is greater than current health, character dies and health is set to 0
            if (currentHealth - damage <= 0) {
                isDead = true;
                currentHealth = 0;
            } else {
                currentHealth -= damage;
            }
            return damage;
            // If the damage is magical
        } else if (damageType.equalsIgnoreCase("Magical")) {
            double damage = incomingDamage * (1 - (baseMagicReductionPercent * equippedArmor.getMagicRedModifier() * equippedArmor.getRarityModifier()));
            // If damage is greater than current health, character dies and health is set to 0
            if (currentHealth - damage <= 0) {
                isDead = true;
                currentHealth = 0;
            } else {
                currentHealth -= damage;
            }
            return damage;
        }
        return 0;
    }
}
