package main.java.characters.damage;
// Imports

import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.basestats.ItemRarityModifiers;
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.CharacterType;
import main.java.characters.abstractions.RangedCharacter;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.abstractions.MailArmor;
import main.java.items.armor.armors.Leather;
import main.java.items.weapons.abstractions.RangedWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.items.weapons.ranged.Bow;

/*
 Rangers
 Are masters of ranged combat. They use a wide arsenal of weapons to dispatch enemies.
*/
public class Ranger extends Character implements RangedCharacter {

    // Active trackers and flags
    private double currentHealth;
    private double currentMaxHealth;
    private Boolean isDead = false;

    // Armor, Weapon
    public Armor equippedArmor = new Leather(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    public Weapon equippedWeapon = new Bow(ItemRarityModifiers.COMMON_RARITY_MODIFIER);

    // Power
    private final double baseAttackPower = CharacterBaseStatsOffensive.RANGER_RANGED_ATTACK_POWER;

    // Constructor
    public Ranger() {
        super(CharacterType.Ranger,
                ArmorType.Mail,
                WeaponCategory.Ranged,
                CharacterBaseStatsDefensive.RANGER_BASE_HEALTH,
                CharacterBaseStatsDefensive.RANGER_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.RANGER_BASE_MAGIC_RES);
        this.currentMaxHealth = baseHealth * equippedArmor.getHealthModifier() * equippedArmor.getRarityModifier();
        this.currentHealth = baseHealth * equippedArmor.getHealthModifier() * equippedArmor.getRarityModifier();
        // When the character is created it has maximum health (base health)
    }

    // Returns current max health
    @Override
    public double getCurrentMaxHealth() {
        return currentMaxHealth;
    }

    // Returns current health
    @Override
    public double getCurrentHealth() {
        return currentHealth;
    }

    // Change health when healed or shielded
    @Override
    public void setCurrentHealth(double incoming) {
        if (currentHealth + incoming >= currentMaxHealth) {
            currentHealth = currentMaxHealth;
        } else {
            currentHealth += incoming;
        }
    }

    // Returns true if character is dead, false otherwise
    @Override
    public Boolean getDead() {
        return isDead;
    }

    // Equipment behaviours

    // Equip new armor, changing max health
    @Override
    public void equipArmor(Armor armor) {
        if (armor instanceof MailArmor) {
            equippedArmor = armor;
            currentMaxHealth = baseHealth * armor.getHealthModifier() * armor.getRarityModifier();
            System.out.println("Equipped " + armor.getArmorType() + " with rarity " + armor.getRarityModifier());
        } else {
            System.out.println("Cannot equip " + armor.getArmorType() + ". Rangers can only equip Mail armor!");
        }
    }

    // Equip a new weapon
    @Override
    public void equipWeapon(Weapon weapon) {
        if (weapon instanceof RangedWeapon) {
            equippedWeapon = weapon;
            System.out.println("Equipped " + weapon.getWeaponType() + " with rarity " + weapon.getItemRarity());
        } else {
            System.out.println("Cannot equip " + weapon.getWeaponType() + " armor. Rangers can only equip a Bow, Crossbow or a Gun!");
        }
    }
    // Character behaviours

    // Attacks with a ranged weapon
    public double attackWithRangedWeapon() {
        return baseAttackPower * equippedWeapon.getPowerModifier() * equippedWeapon.getItemRarity(); // Replaced with actual damage amount based on calculations
    }

    // Character takes damage, either physical or magical
    @Override
    public double takeDamage(double incomingDamage, String damageType) {
        // If the damage is physical
        if (damageType.equalsIgnoreCase("Physical")) {
            double damage = incomingDamage * (1 - (basePhysReductionPercent * equippedArmor.getPhysRedModifier() * equippedArmor.getRarityModifier()));
            // If damage is greater than current health, character dies and health is set to 0
            if (currentHealth - damage <= 0) {
                isDead = true;
                currentHealth = 0;
            } else {
                currentHealth -= damage;
            }
            return damage;
            // If the damage is magical
        } else if (damageType.equalsIgnoreCase("Magical")) {
            double damage = incomingDamage * (1 - (baseMagicReductionPercent * equippedArmor.getMagicRedModifier() * equippedArmor.getRarityModifier()));
            // If damage is greater than current health, character dies and health is set to 0
            if (currentHealth - damage <= 0) {
                isDead = true;
                currentHealth = 0;
            } else {
                currentHealth -= damage;
            }
            return damage;
        }
        return 0;
    }
}
