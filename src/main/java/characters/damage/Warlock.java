package main.java.characters.damage;
// Imports

import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.basestats.ItemRarityModifiers;
import main.java.characters.abstractions.CasterCharacter;
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.CharacterType;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.abstractions.ClothArmor;
import main.java.items.armor.armors.Cloth;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.items.weapons.magic.Staff;
import main.java.spells.abstractions.DamagingSpell;
import main.java.spells.damaging.ChaosBolt;

/*
 Warlocks are masters of chaos, entropy, and death. They were once mages but were corrupted by power.
 They can conjure up pure chaos energy to destroy their enemies.
*/
public class Warlock extends Character implements CasterCharacter {

    // Active trackers and flags
    private double currentHealth;
    private double currentMaxHealth;
    private Boolean isDead = false;

    // Armor, Weapon and Spell
    public Armor equippedArmor = new Cloth(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    public Weapon equippedWeapon = new Staff(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    private final DamagingSpell damagingSpell = new ChaosBolt();

    // Power
    private final double baseMagicPower = CharacterBaseStatsOffensive.WARLOCK_MAGIC_POWER;

    public Warlock() {
        super(CharacterType.Warlock,
                ArmorType.Cloth,
                WeaponCategory.Magic,
                CharacterBaseStatsDefensive.WARLOCK_BASE_HEALTH,
                CharacterBaseStatsDefensive.WARLOCK_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.WARLOCK_BASE_MAGIC_RES);
        this.currentMaxHealth = baseHealth * equippedArmor.getHealthModifier() * equippedArmor.getRarityModifier();
        this.currentHealth = baseHealth * equippedArmor.getHealthModifier() * equippedArmor.getRarityModifier();
    }

    // Returns current max health
    @Override
    public double getCurrentMaxHealth() {
        return currentMaxHealth;
    }

    // Returns current health
    @Override
    public double getCurrentHealth() {
        return currentHealth;
    }

    // Change health when healed or shielded
    @Override
    public void setCurrentHealth(double incoming) {
        if (currentHealth + incoming >= currentMaxHealth) {
            currentHealth = currentMaxHealth;
        } else {
            currentHealth += incoming;
        }
    }

    // Returns true if character is dead, false otherwise
    @Override
    public Boolean getDead() {
        return isDead;
    }

    // Equipment behaviours

    // Equip new armor, changing max health
    @Override
    public void equipArmor(Armor armor) {
        if (armor instanceof ClothArmor) {
            equippedArmor = armor;
            System.out.println("Equipped " + armor.getArmorType() + " with rarity " + armor.getRarityModifier());
        } else {
            System.out.println("Cannot equip " + armor.getArmorType() + " armor. Warlocks can only equip Cloth armor!");
        }
    }

    // Equip a new weapon
    @Override
    public void equipWeapon(Weapon weapon) {
        if (weapon instanceof MagicWeapon) {
            equippedWeapon = weapon;
            System.out.println("Equipped " + weapon.getWeaponType() + " with rarity " + weapon.getItemRarity());
        } else {
            System.out.println("Cannot equip " + weapon.getWeaponType() + ". Warlocks can only equip a Staff or a Wand!");
        }
    }
    // Character behaviours

    // Casts a damaging spell
    public double castDamagingSpell() {
        return baseMagicPower * equippedWeapon.getPowerModifier() * damagingSpell.getSpellDamageModifier() * equippedWeapon.getItemRarity();
    }

    // Character takes damage, either physical or magical
    @Override
    public double takeDamage(double incomingDamage, String damageType) {
        // If the damage is physical
        if (damageType.equalsIgnoreCase("Physical")) {
            double damage = incomingDamage * (1 - (basePhysReductionPercent * equippedArmor.getPhysRedModifier() * equippedArmor.getRarityModifier()));
            // If damage is greater than current health, character dies and health is set to 0
            if (currentHealth - damage <= 0) {
                isDead = true;
                currentHealth = 0;
            } else {
                currentHealth -= damage;
            }
            return damage;
            // If the damage is magical
        } else if (damageType.equalsIgnoreCase("Magical")) {
            double damage = incomingDamage * (1 - (baseMagicReductionPercent * equippedArmor.getMagicRedModifier() * equippedArmor.getRarityModifier()));
            // If damage is greater than current health, character dies and health is set to 0
            if (currentHealth - damage <= 0) {
                isDead = true;
                currentHealth = 0;
            } else {
                currentHealth -= damage;
            }
            return damage;
        }
        return 0;
    }
}
