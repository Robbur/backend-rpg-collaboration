package main.java.characters.support;
// Imports

import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.ItemRarityModifiers;
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.CharacterType;
import main.java.characters.abstractions.SupportCharacter;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.armors.Leather;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.items.weapons.magic.Staff;
import main.java.spells.abstractions.HealingSpell;
import main.java.spells.healing.Regrowth;

/*
 Class description:
 ------------------
 Druids are spell casters who use nature based magic to aid their allies in battle.
 They can heal their allies or protect them using the forces of nature.
 As a support class they only have defensive stats.
*/
public class Druid extends Character implements SupportCharacter {

    // Active trackers and flags
    private double currentHealth;
    private double currentMaxHealth;
    private Boolean isDead = false;

    // Armor, Weapon and Spells
    public Armor equippedArmor = new Leather(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    public Weapon equippedWeapon = new Staff(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    private HealingSpell healingSpell = new Regrowth();

    // Constructors
    public Druid() {
        super(CharacterType.Druid,
                ArmorType.Leather,
                WeaponCategory.Magic,
                CharacterBaseStatsDefensive.DRUID_BASE_HEALTH,
                CharacterBaseStatsDefensive.DRUID_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.DRUID_BASE_MAGIC_RES);
        this.currentMaxHealth = baseHealth * equippedArmor.getHealthModifier() * equippedArmor.getRarityModifier();
        this.currentHealth = baseHealth * equippedArmor.getHealthModifier() * equippedArmor.getRarityModifier();
        // When the character is created it has maximum health (base health)
    }

    // Returns current max health
    @Override
    public double getCurrentMaxHealth() {
        return currentMaxHealth;
    }

    // Returns current health
    @Override
    public double getCurrentHealth() {
        return currentHealth;
    }

    // Change health when healed or shielded
    @Override
    public void setCurrentHealth(double incoming) {
        if (currentHealth + incoming >= currentMaxHealth) {
            currentHealth = currentMaxHealth;
        } else {
            currentHealth += incoming;
        }
    }

    // Returns true if character is dead, false otherwise
    @Override
    public Boolean getDead() {
        return isDead;
    }

    // Equipment behaviours

    // Equip new armor, changing max health
    @Override
    public void equipArmor(Armor armor) {
        if (armor instanceof Leather) {
            equippedArmor = armor;
            currentMaxHealth = baseHealth * armor.getHealthModifier() * armor.getRarityModifier();
            System.out.println("Equipped " + armor.getArmorType() + " with rarity " + armor.getRarityModifier());
        } else {
            System.out.println("Cannot equip " + armor.getArmorType() + " armor. Druids can only equip Leather armor!");
        }
    }

    // Equip a new weapon
    @Override
    public void equipWeapon(Weapon weapon) {
        if (weapon instanceof MagicWeapon) {
            equippedWeapon = weapon;
            System.out.println("Equipped " + weapon.getWeaponType() + " with rarity " + weapon.getItemRarity());
        } else {
            System.out.println("Cannot equip " + weapon.getWeaponType() + ". Druids can only equip a Staff or a Wand!");
        }
    }

    // Character behaviours

    // Heals a party member
    public double healPartyMember(Character character) {
        double healingAmount = healingSpell.getHealingAmount() * equippedWeapon.getPowerModifier() * equippedWeapon.getItemRarity();
        character.setCurrentHealth(healingAmount);
        return healingAmount;
    }

    // Character takes damage, either physical or magical
    @Override
    public double takeDamage(double incomingDamage, String damageType) {
        // If the damage is physical
        if (damageType.equalsIgnoreCase("Physical")) {
            double damage = incomingDamage * (1 - (basePhysReductionPercent * equippedArmor.getPhysRedModifier() * equippedArmor.getRarityModifier()));
            // If damage is greater than current health, character dies and health is set to 0
            if (currentHealth - damage <= 0) {
                isDead = true;
                currentHealth = 0;
            } else {
                currentHealth -= damage;
            }
            return damage;
            // If the damage is magical
        } else if (damageType.equalsIgnoreCase("Magical")) {
            double damage = incomingDamage * (1 - (baseMagicReductionPercent * equippedArmor.getMagicRedModifier() * equippedArmor.getRarityModifier()));
            // If damage is greater than current health, character dies and health is set to 0
            if (currentHealth - damage <= 0) {
                isDead = true;
                currentHealth = 0;
            } else {
                currentHealth -= damage;
            }
            return damage;
        }
        return 0;
    }
}
