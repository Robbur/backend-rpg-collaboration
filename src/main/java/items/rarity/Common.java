package main.java.items.rarity;

import main.java.consolehelpers.Color;

public class Common {
    // Stat modifier
    private double powerModifier = 1;
    // Color for display purposes
    private String itemRarityColor = Color.WHITE;

    // Public properties
    public double powerModifier() {
        return powerModifier;
    }

    public String getItemRarityColor() {
        return itemRarityColor;
    }
}
