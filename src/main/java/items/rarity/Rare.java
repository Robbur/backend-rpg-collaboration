package main.java.items.rarity;

import main.java.consolehelpers.Color;

public class Rare {
    // Stat modifier
    private double powerModifier = 1.4;
    // Color for display purposes
    private String itemRarityColor = Color.BLUE;

    // Public properties
    public double powerModifier() {
        return powerModifier;
    }

    public String getItemRarityColor() {
        return itemRarityColor;
    }
}
