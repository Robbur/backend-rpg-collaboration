package main.java.items.armor.abstractions;

public interface MailArmor {

    double getHealthModifier();

    double getPhysRedModifier();

    double getMagicRedModifier();

    double getRarityModifier();
}
