package main.java.items.armor.abstractions;

public interface ClothArmor {

    double getHealthModifier();

    double getPhysRedModifier();

    double getMagicRedModifier();

    double getRarityModifier();
}
