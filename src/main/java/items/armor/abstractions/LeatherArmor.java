package main.java.items.armor.abstractions;

public interface LeatherArmor {

    double getHealthModifier();

    double getPhysRedModifier();

    double getMagicRedModifier();

    double getRarityModifier();
}
