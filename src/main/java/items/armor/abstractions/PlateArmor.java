package main.java.items.armor.abstractions;

public interface PlateArmor {

    double getHealthModifier();

    double getPhysRedModifier();

    double getMagicRedModifier();

    double getRarityModifier();
}
