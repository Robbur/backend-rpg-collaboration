package main.java.items.armor.abstractions;

public abstract class Armor{
    // Stat modifiers
    protected double healthModifier;
    protected double physRedModifier;
    protected double magicRedModifier;
    // Rarity
    protected double rarityModifier;

    public Armor(double healthModifier, double physRedModifier, double magicRedModifier, double rarityModifier) {
        this.healthModifier = healthModifier;
        this.physRedModifier = physRedModifier;
        this.magicRedModifier = magicRedModifier;
        this.rarityModifier = rarityModifier;
    }

    public abstract double getHealthModifier();

    public abstract double getPhysRedModifier();

    public abstract double getMagicRedModifier();

    public abstract double getRarityModifier();

    public abstract ArmorType getArmorType();
}
