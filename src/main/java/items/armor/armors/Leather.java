package main.java.items.armor.armors;

import main.java.basestats.ArmorStatsModifiers;
import main.java.basestats.ItemRarityModifiers;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.abstractions.LeatherArmor;

public class Leather extends Armor implements LeatherArmor {

    // Armor Type
    private final ArmorType armorType = ArmorType.Leather;
    
    // Constructors
    public Leather() {
        super(ArmorStatsModifiers.LEATHER_HEALTH_MODIFIER,
                ArmorStatsModifiers.LEATHER_PHYS_RED_MODIFIER,
                ArmorStatsModifiers.LEATHER_MAGIC_RED_MODIFIER,
                ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    }

    public Leather(double itemRarityModifier) {
        super(ArmorStatsModifiers.LEATHER_HEALTH_MODIFIER,
                ArmorStatsModifiers.LEATHER_PHYS_RED_MODIFIER,
                ArmorStatsModifiers.LEATHER_MAGIC_RED_MODIFIER,
                itemRarityModifier);
    }

    @Override
    public double getHealthModifier() {
        return healthModifier;
    }

    @Override
    public double getPhysRedModifier() {
        return physRedModifier;
    }

    @Override
    public double getMagicRedModifier() {
        return magicRedModifier;
    }

    @Override
    public double getRarityModifier() {
        return rarityModifier;
    }

    @Override
    public ArmorType getArmorType() {
        return armorType;
    }
}
