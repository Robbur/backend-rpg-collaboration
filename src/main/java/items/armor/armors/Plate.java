package main.java.items.armor.armors;

import main.java.basestats.ArmorStatsModifiers;
import main.java.basestats.ItemRarityModifiers;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.abstractions.PlateArmor;

public class Plate extends Armor implements PlateArmor {

    // Armor Type
    private final ArmorType armorType = ArmorType.Plate;
    
    // Constructors
    public Plate() {
        super(ArmorStatsModifiers.PLATE_HEALTH_MODIFIER,
                ArmorStatsModifiers.PLATE_PHYS_RED_MODIFIER,
                ArmorStatsModifiers.PLATE_MAGIC_RED_MODIFIER,
                ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    }

    public Plate(double itemRarityModifier) {
        super(ArmorStatsModifiers.PLATE_HEALTH_MODIFIER,
                ArmorStatsModifiers.PLATE_PHYS_RED_MODIFIER,
                ArmorStatsModifiers.PLATE_MAGIC_RED_MODIFIER,
                itemRarityModifier);
    }

    @Override
    public double getHealthModifier() {
        return healthModifier;
    }

    @Override
    public double getPhysRedModifier() {
        return physRedModifier;
    }

    @Override
    public double getMagicRedModifier() {
        return magicRedModifier;
    }

    @Override
    public double getRarityModifier() {
        return rarityModifier;
    }

    @Override
    public ArmorType getArmorType() {
        return armorType;
    }
}
