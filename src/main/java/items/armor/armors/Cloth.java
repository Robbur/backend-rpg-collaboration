package main.java.items.armor.armors;

import main.java.basestats.ArmorStatsModifiers;
import main.java.basestats.ItemRarityModifiers;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.abstractions.ClothArmor;

public class Cloth extends Armor implements ClothArmor {

    // Armor Type
    private final ArmorType armorType = ArmorType.Cloth;

    // Constructors
    public Cloth() {
        super(ArmorStatsModifiers.CLOTH_HEALTH_MODIFIER,
                ArmorStatsModifiers.CLOTH_PHYS_RED_MODIFIER,
                ArmorStatsModifiers.CLOTH_MAGIC_RED_MODIFIER,
                ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    }

    public Cloth(double itemRarityModifier) {
        super(ArmorStatsModifiers.CLOTH_HEALTH_MODIFIER,
                ArmorStatsModifiers.CLOTH_PHYS_RED_MODIFIER,
                ArmorStatsModifiers.CLOTH_MAGIC_RED_MODIFIER,
                itemRarityModifier);
    }

    @Override
    public double getHealthModifier() {
        return healthModifier;
    }

    @Override
    public double getPhysRedModifier() {
        return physRedModifier;
    }

    @Override
    public double getMagicRedModifier() {
        return magicRedModifier;
    }

    @Override
    public double getRarityModifier() {
        return rarityModifier;
    }

    @Override
    public ArmorType getArmorType() {
        return armorType;
    }
}
