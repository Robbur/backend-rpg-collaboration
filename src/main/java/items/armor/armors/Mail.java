package main.java.items.armor.armors;

import main.java.basestats.ArmorStatsModifiers;
import main.java.basestats.ItemRarityModifiers;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.abstractions.MailArmor;

public class Mail extends Armor implements MailArmor {

    // Armor Type
    private final ArmorType armorType = ArmorType.Mail;
    
    // Constructors
    public Mail() {
        super(ArmorStatsModifiers.MAIL_HEALTH_MODIFIER,
                ArmorStatsModifiers.MAIL_PHYS_RED_MODIFIER,
                ArmorStatsModifiers.MAIL_MAGIC_RED_MODIFIER,
                ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    }

    public Mail(double itemRarityModifier) {
        super(ArmorStatsModifiers.MAIL_HEALTH_MODIFIER,
                ArmorStatsModifiers.MAIL_PHYS_RED_MODIFIER,
                ArmorStatsModifiers.MAIL_MAGIC_RED_MODIFIER,
                itemRarityModifier);
    }

    @Override
    public double getHealthModifier() {
        return healthModifier;
    }

    @Override
    public double getPhysRedModifier() {
        return physRedModifier;
    }

    @Override
    public double getMagicRedModifier() {
        return magicRedModifier;
    }

    @Override
    public double getRarityModifier() {
        return rarityModifier;
    }

    @Override
    public ArmorType getArmorType() {
        return armorType;
    }
}
