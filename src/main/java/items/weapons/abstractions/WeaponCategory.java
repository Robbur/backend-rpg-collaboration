package main.java.items.weapons.abstractions;

public enum WeaponCategory {
    Magic,
    BladeWeapon,
    BluntWeapon,
    Ranged
}
