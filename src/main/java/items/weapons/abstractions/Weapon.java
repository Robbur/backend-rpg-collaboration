package main.java.items.weapons.abstractions;

public abstract class Weapon{
    // Weapon rarity modifier
    protected double itemRarityModifier;

    // Constructor
    public Weapon(double itemRarityModifier) {
        this.itemRarityModifier = itemRarityModifier;
    }

    // Get weapon rarity modifier
    public double getItemRarity() {
        return itemRarityModifier;
    }

    public abstract double getPowerModifier();

    public abstract WeaponType getWeaponType();
}
