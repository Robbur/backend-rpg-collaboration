package main.java.items.weapons.melee;

import main.java.basestats.ItemRarityModifiers;
import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.BluntWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;

public class Mace extends Weapon implements BluntWeapon {

    // Weapon Type
    private final WeaponType weaponType = WeaponType.Mace;

    // Constructors
    public Mace() {
        super(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    }

    public Mace(double itemRarityModifier) {
        super(itemRarityModifier);
    }

    @Override
    public double getPowerModifier() {
        // Stat modifiers
        return WeaponStatsModifiers.MACE_ATTACK_MOD;
    }

    @Override
    public WeaponType getWeaponType() {
        return weaponType;
    }
}
