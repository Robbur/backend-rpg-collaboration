package main.java.items.weapons.melee;

import main.java.basestats.ItemRarityModifiers;
import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.BluntWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;

public class Hammer extends Weapon implements BluntWeapon {

    // Weapon Type
    private final WeaponType weaponType = WeaponType.Hammer;

    // Constructors
    public Hammer() {
        super(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    }

    public Hammer(double itemRarityModifier) {
        super(itemRarityModifier);
    }

    @Override
    public double getPowerModifier() {
        // Stat modifiers
        return WeaponStatsModifiers.HAMMER_ATTACK_MOD;
    }

    @Override
    public WeaponType getWeaponType() {
        return weaponType;
    }
}
