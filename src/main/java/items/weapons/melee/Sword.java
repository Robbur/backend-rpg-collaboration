package main.java.items.weapons.melee;

import main.java.basestats.ItemRarityModifiers;
import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.BladedWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;

public class Sword extends Weapon  implements BladedWeapon {

    // Weapon Type
    private final WeaponType weaponType = WeaponType.Sword;

    // Constructors
    public Sword() {
        super(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    }

    public Sword(double itemRarityModifier) {
        super(itemRarityModifier);
    }

    @Override
    public double getPowerModifier() {
        // Stat modifiers
        return WeaponStatsModifiers.SWORD_ATTACK_MOD;
    }

    @Override
    public WeaponType getWeaponType() {
        return weaponType;
    }
}
