package main.java.items.weapons.melee;

import main.java.basestats.ItemRarityModifiers;
import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.BladedWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;

public class Axe extends Weapon implements BladedWeapon {

    // Weapon Type
    private final WeaponType weaponType = WeaponType.Axe;

    // Constructors
    public Axe() {
        super(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    }

    public Axe(double itemRarityModifier) {
        super(itemRarityModifier);
    }

    @Override
    public double getPowerModifier() {
        // Stat modifiers
        return WeaponStatsModifiers.AXE_ATTACK_MOD;
    }

    @Override
    public WeaponType getWeaponType() {
        return weaponType;
    }
}
