package main.java.items.weapons.magic;

import main.java.basestats.ItemRarityModifiers;
import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;

public class Wand extends Weapon implements MagicWeapon {

    // Weapon Type
    private final WeaponType weaponType = WeaponType.Wand;

    // Constructors
    public Wand() {
        super(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    }

    public Wand(double itemRarityModifier) {
        super(itemRarityModifier);
    }

    @Override
    public double getPowerModifier() {
        // Stat modifiers
        return WeaponStatsModifiers.WAND_MAGIC_MOD;
    }
    @Override
    public WeaponType getWeaponType() {
        return weaponType;
    }
}
