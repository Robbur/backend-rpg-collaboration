package main.java.items.weapons.magic;

import main.java.basestats.ItemRarityModifiers;
import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;

public class Staff extends Weapon implements MagicWeapon {

    // Weapon Type
    private final WeaponType weaponType = WeaponType.Staff;

    // Constructors
    public Staff() {
        super(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    }

    public Staff(double itemRarityModifiers) {
        super(itemRarityModifiers);
    }

    @Override
    public double getPowerModifier() {
        // Stat modifiers
        return WeaponStatsModifiers.STAFF_MAGIC_MOD;
    }

    @Override
    public WeaponType getWeaponType() {
        return weaponType;
    }
}
