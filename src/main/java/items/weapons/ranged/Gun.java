package main.java.items.weapons.ranged;

import main.java.basestats.ItemRarityModifiers;
import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.RangedWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;

public class Gun extends Weapon  implements RangedWeapon {

    // Weapon Type
    private final WeaponType weaponType = WeaponType.Gun;

    // Constructors
    public Gun() {
        super(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    }

    public Gun(double itemRarityModifier) {
        super(itemRarityModifier);
    }

    @Override
    public double getPowerModifier() {
        // Stat modifiers
        return WeaponStatsModifiers.GUN_ATTACK_MOD;
    }

    @Override
    public WeaponType getWeaponType() {
        return weaponType;
    }
}
