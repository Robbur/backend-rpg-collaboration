package main.java.items.weapons.ranged;

import main.java.basestats.ItemRarityModifiers;
import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.RangedWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;

public class Bow extends Weapon implements RangedWeapon {

    // Weapon Type
    private final WeaponType weaponType = WeaponType.Bow;

    // Constructors
    public Bow() {
        super(ItemRarityModifiers.COMMON_RARITY_MODIFIER);
    }

    public Bow(double itemRarityModifier) {
        super(itemRarityModifier);
    }

    @Override
    public double getPowerModifier() {
        // Stat modifiers
        return WeaponStatsModifiers.BOW_ATTACK_MOD;
    }

    @Override
    public WeaponType getWeaponType() {
        return weaponType;
    }
}
