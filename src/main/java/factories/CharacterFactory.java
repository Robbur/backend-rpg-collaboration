package main.java.factories;
// Imports

import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.CharacterType;
import main.java.characters.damage.*;
import main.java.characters.support.Druid;
import main.java.characters.support.Priest;

/*
 This factory exists to be responsible for creating new characters.
*/

public class CharacterFactory {
    public Character getCharacter(CharacterType characterType) {
        switch (characterType) {
            case Mage:
                return new Mage();
            case Paladin:
                return new Paladin();
            case Ranger:
                return new Ranger();
            case Rogue:
                return new Rogue();
            case Warlock:
                return new Warlock();
            case Warrior:
                return new Warrior();
            case Druid:
                return new Druid();
            case Priest:
                return new Priest();
            default:
                return null;
        }
    }
}
