package main.java.factories;
// Imports

import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.armors.Cloth;
import main.java.items.armor.armors.Leather;
import main.java.items.armor.armors.Mail;
import main.java.items.armor.armors.Plate;

/*
 This factory exists to be responsible for creating new Armor.
*/
public class ArmorFactory {
    public Armor getArmor(ArmorType armorType, double itemRarityModifier) {
        switch (armorType) {
            case Cloth:
                return new Cloth(itemRarityModifier);
            case Leather:
                return new Leather(itemRarityModifier);
            case Mail:
                return new Mail(itemRarityModifier);
            case Plate:
                return new Plate(itemRarityModifier);
            default:
                return null;
        }
    }
}
