package main.java.factories;

import main.java.spells.abstractions.Spell;
import main.java.spells.abstractions.SpellType;
import main.java.spells.damaging.ArcaneMissile;
import main.java.spells.damaging.ChaosBolt;
import main.java.spells.healing.Regrowth;
import main.java.spells.healing.SwiftMend;
import main.java.spells.shielding.Barrier;
import main.java.spells.shielding.Rapture;

public class SpellFactory {
    public Spell getSpell(SpellType spellType) {
        switch (spellType) {
            case ArcaneMissile:
                return new ArcaneMissile();
            case Barrier:
                return new Barrier();
            case ChaosBolt:
                return new ChaosBolt();
            case Regrowth:
                return new Regrowth();
            case SwiftMend:
                return new SwiftMend();
            case Rapture:
                return new Rapture();
            default:
                return null;
        }
    }
}
