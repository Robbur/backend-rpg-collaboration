# RPG-Collaboration

This is a console RPG game that allows you to create different characters, and equip them with different
weapons, spells and armor.

Each character can perform an attack or heal.
Each character can be equipped with a new weapon, or a new armor piece. 
Equipping new armor or weapon will increase max health and damage/healing/shielding done.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

You can start by downloading the project in a .zip file, or cloning the project using the terminal.

### Prerequisites

What things you need to install the software and how to install them

Install the latest version of Java JDK: https://www.oracle.com/java/technologies/javase-jdk14-downloads.html

### Running the project

Open the project in an IDE of your choice and run the Main.java class.

The Main.java class has many examples of what you can do with characters, attacks/heals and weapons/armor.

## Authors

* **Robin Burø** - *Initial work* - [Robbur](https://gitlab.com/Robbur)

